;; -*- lexical-binding: t -*-

(with-eval-after-load 'hl-prog-extra
  (setq hl-prog-extra-list

        ;; Extend the default list to include the bug tracker ID.
        (append
         (list
          ;; Match `some.text` as a constant.
          '("`[^`\n]+`" 0 comment font-lock-constant-face)

          '("[#!][[:digit:]]+\\>" 0 (string comment) font-lock-delimit-face))
         hl-prog-extra-list))
  (hl-prog-extra-refresh))

;; Local dictionary.
(unless (boundp 'sidecar-locals-blender-c-wordlist)
  (defvar sidecar-locals-blender-c-wordlist (list "makefile")))
(setq spell-fu-buffer-session-localwords sidecar-locals-blender-c-wordlist)
(spell-fu-buffer-session-localwords-update)

;; Local Variables:
;; fill-column: 99
;; indent-tabs-mode: nil
;; End:
