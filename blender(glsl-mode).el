;; -*- lexical-binding: t -*-

;; Shared code for C-like languages.
(let ((path (file-name-directory load-file-name)))
  (load (concat path "blender(c-mode-generic).el") :nomessage t))

;; Local Variables:
;; fill-column: 99
;; indent-tabs-mode: nil
;; End:
