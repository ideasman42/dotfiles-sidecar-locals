;; -*- lexical-binding: t -*-

(with-eval-after-load 'hl-prog-extra
  (setq hl-prog-extra-list
        ;; Extend the default list to include the bug tracker ID.
        (append
         (list
          ;; Match `some.text` as a constant.
          '("`[^`\n]+`" 0 comment-only font-lock-constant-face)
          ;; Match ``some.text`` as a constant.
          '("``[^`\n]+``" 0 string-doc font-lock-constant-face)

          '("[#!][[:digit:]]+\\>" 0 (string comment) font-lock-delimit-face)

          ;; Match *some text* as bold.
          (list
           "\\*\\_<[^\\*]+\\_>\\*" 0 'comment-only '(:inherit font-lock-comment-face :weight bold))
          (list "\\*\\_<[^\\*]+\\_>\\*" 0 'comment-doc '(:inherit font-lock-doc-face :weight bold))

          ;; Alerts.
          (list
           "\\<\\(NOTE\\)\\>\\((\\([^)+]+\\))\\)?" '(0 3) 'comment
           (list
            '(:background "#006000" :foreground "#FFFFFF")
            '(:background "#006000" :foreground "#BBBBBB")))

          (list
           "\\<\\(TODO\\|WORKAROUND\\)\\>\\((\\([^)+]+\\))\\)?" '(0 3) 'comment
           (list
            '(:background "#707000" :foreground "#FFFFFF")
            '(:background "#707000" :foreground "#BBBBBB")))

          (list
           "\\<\\(FIXME\\|XXX\\|WARNING\\)\\>\\((\\([^)+]+\\))\\)?" '(0 3) 'comment
           (list
            '(:background "#800000" :foreground "#FFFFFF")
            '(:background "#800000" :foreground "#BBBBBB")))

          ;; URL.
          (list "\\<https?://[^[:blank:]]*" 0 'comment 'font-lock-constant-face)
          ;; Email.
          (list "<\\([[:alnum:]\\._-]+@[[:alnum:]\\._-]+\\)>" 1 'comment 'font-lock-constant-face))
         (hl-prog-extra-preset)))
  (hl-prog-extra-refresh))

;; Local Variables:
;; fill-column: 99
;; indent-tabs-mode: nil
;; End:
