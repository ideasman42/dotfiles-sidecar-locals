;; -*- lexical-binding: t -*-

;; Use for C-like languages (C/C++/GLSL).
;; Show doc-strings inline.
(doc-show-inline-mode)

(with-eval-after-load 'clang-format
  (setq clang-format-executable
        (file-name-concat (sidecar-locals-root) "lib" "linux_x64" "llvm" "bin" "clang-format")))

(with-eval-after-load 'hl-prog-extra
  (setq hl-prog-extra-list
        ;; Extend the default list to include the bug tracker ID.
        (append
         (list
          ;; Match `some.text` as a constant.
          (list "`[^`\n]+`" 0 'comment 'font-lock-doc-markup-face)
          ;; Match 'some.text' as a constant.
          ;; '("'\\_<[^'\n]+\\_>'" 0 comment font-lock-doc-markup-face)
          (list "'\\_<[^'[:blank:]]+\\_>'" 0 'comment 'font-lock-doc-markup-face)

          ;; BEGIN DOXYGEN.
          (list
           "\\(\\\\param[[:blank:]]+\\)\\([a-zA-Z0-9_]+\\)"
           '(1 2)
           'comment-doc
           (list 'font-lock-doc-markup-face 'font-lock-delimit-face))
          (list
           "\\(\\\\name[[:blank:]]+\\)\\([^\n]+\\)"
           '(1 2)
           'comment-doc
           (list 'font-lock-doc-markup-face '(:inherit font-lock-doc-face :weight bold)))

          (list "\\\\\\([a-z]+\\|[[:punct:]]+\\)" 0 'comment-doc 'font-lock-doc-markup-face)
          ;; END DOXYGEN.

          ;; Match *some text* as bold.
          (list
           "\\*\\_<[^\\*]+\\_>\\*" 0 'comment-only '(:inherit font-lock-comment-face :weight bold))
          (list "\\*\\_<[^\\*]+\\_>\\*" 0 'comment-doc '(:inherit font-lock-doc-face :weight bold))

          ;; Match http://xyz (URL)
          (list "\\<http[s]?://[^[:blank:]]*" 0 'comment 'font-lock-doc-markup-face)
          ;; Match <email@name.foo> email address.
          (list "<\\([[:alnum:]\\._-]+@[a-z0-9._-]+\\)>" 1 'comment 'font-lock-doc-markup-face)

          ;; Alerts.
          (list
           "\\<\\(NOTE\\)\\>\\((\\([^)+]+\\))\\)?" '(0 3) 'comment
           (list
            '(:background "#006000" :foreground "#FFFFFF")
            '(:background "#006000" :foreground "#BBBBBB")))

          (list
           "\\<\\(TODO\\|WORKAROUND\\)\\>\\((\\([^)+]+\\))\\)?" '(0 3) 'comment
           (list
            '(:background "#707000" :foreground "#FFFFFF")
            '(:background "#707000" :foreground "#BBBBBB")))

          (list
           "\\<\\(FIXME\\|XXX\\|WARNING\\)\\>\\((\\([^)+]+\\))\\)?" '(0 3) 'comment
           (list
            '(:background "#800000" :foreground "#FFFFFF")
            '(:background "#800000" :foreground "#BBBBBB")))

          ;; Code highlighting.
          (list "\\<true\\>" 0 nil '(:background "#006600"))
          (list "\\<false\\>" 0 nil '(:background "#660000"))

          ;; Highlighting for punctuation/delimiters.
          (list "\\([\]\[}{)(:;]\\)" 0 nil 'font-lock-delimit-face)
          ;; Simple/stupid function call highlighting `function_call(...)`
          ;; '("\\([_a-zA-Z][_a-zA-Z0-9]*\\)\s*(" 1 nil font-lock-function-name-face)
          ;; Remove white-space, avoids highlighting `if (` ... weak since it takes white-space
          ;; into account, but in practice it's OK.

          ;; '("\\([_a-zA-Z][_a-zA-Z0-9]*\\)(" 1 nil font-lock-function-name-face)

          (list "[#!][[:digit:]]+\\>" 0 'comment 'font-lock-delimit-face)

          ;; #ID as constant.
          (list "#[[:alnum:].*:!<>()_-]+" 0 'comment 'font-lock-doc-markup-face)

          ;; Draw extra attention to notes and warnings.
          (list "\\\\note\\>" 0 'comment '(:background "#008800" :foreground "#FFFFFF"))
          (list "\\\\warning\\>" 0 'comment '(:background "#880000" :foreground "#FFFFFF")))

         (hl-prog-extra-preset)))
  (hl-prog-extra-refresh))

(with-eval-after-load 'prog-face-refine
  (setq prog-face-refine-list
        (list
         (list (concat (regexp-quote "/**") "[^*]") 'comment 'font-lock-doc-face)
         (list (concat (regexp-quote "//") "[^/]") 'comment 'font-lock-doc-markup-face)))
  (prog-face-refine-refresh))

;; Show the doxygen section in the mode-line (when idle).
(with-eval-after-load 'mode-line-idle
  (when (member
         major-mode
         (list
          'c-mode 'c++-mode 'glsl-mode
          ;; Tree-sitter modes.
          'c-ts-mode 'c++-ts-mode))
    (setq-local my-mode-line-idle
                (append
                 my-mode-line-idle
                 (list
                  '(:eval
                    (save-match-data
                      (save-excursion
                        (when (re-search-backward "\\(\\\\}\\|\\\\{\\)" nil t 1)
                          (pcase (char-after (+ 1 (point)))
                            (?{ ;; Section Start
                             (let ((section-start-pt (point)))
                               (when (re-search-backward "\\/\\*" nil t 1) ;; find start of comment
                                 (when (re-search-forward "\\name[[:blank:]]+\\(.*\\)"
                                                          section-start-pt
                                                          t
                                                          1)
                                   (propertize (match-string-no-properties 1)
                                               'face
                                               'font-lock-comment-face)))))
                            (?} ;; Section End
                             ;; Do nothing, we're not in a section.
                             nil)))))))))))

(with-eval-after-load 'counsel
  (let ((extra-args
         (cond
          ((member
            major-mode
            (list
             'c-mode 'c++-mode
             ;; Tree-sitter modes.
             'c-ts-mode 'c++-ts-mode))
           ;; Ignore these directories.
           (list
            "-g=!extern/**"
            "-g=!intern/atomic/**"
            "-g=!intern/audaspace/**"
            ;; "-g=!intern/cycles/**"
            "-g=!intern/dualcon/**"
            "-g=!intern/eigen/**"
            "-g=!intern/ffmpeg/**"
            "-g=!intern/glew-mx/**"
            "-g=!intern/guardedalloc/**"
            "-g=!intern/iksolver/**"
            "-g=!intern/itasc/**"
            "-g=!intern/libmv/**"
            "-g=!intern/locale/**"
            "-g=!intern/mantaflow/**"
            "-g=!intern/memutil/**"
            "-g=!intern/mikktspace/**"
            "-g=!intern/opencolorio/**"
            "-g=!intern/opensubdiv/**"
            "-g=!intern/openvdb/**"
            "-g=!intern/rigidbody/**"
            "-g=!intern/sky/**"
            "-g=!intern/utfconv/**"
            "-g=!release/**"
            "-g=!build_files/**"
            "-g=!doc/**")))))

    (setq-local counsel-rg-base-command
                (append
                 (list (car counsel-rg-base-command)) extra-args (cdr counsel-rg-base-command)))))

(setq-local my-generic-run
            (lambda ()
              (interactive)
              (let ((debug-session (dap--cur-session)))
                (cond
                 ((and debug-session (dap--session-running debug-session))
                  (call-interactively 'dap-continue))
                 (t
                  (require 'dap-gdb-lldb)
                  (dap-gdb-lldb-setup)
                  (my-with-advice
                   'dap--completing-read
                   :override (lambda (&rest) nil)
                   (dap-debug
                    (list
                     :type "gdb"
                     :request "launch"
                     :arguments (list "--factory-startup")
                     :cwd (sidecar-locals-root)
                     :target (concat (sidecar-locals-root) "blender.bin")
                     :name "GDB::Blender"))))))))

;; Local dictionary.
(unless (boundp 'sidecar-locals-blender-c-wordlist)
  (defvar sidecar-locals-blender-c-wordlist (list "args" "macos" "nullptr" "wayland")))
(setq spell-fu-buffer-session-localwords sidecar-locals-blender-c-wordlist)
(spell-fu-buffer-session-localwords-update)

;; Local Variables:
;; fill-column: 99
;; indent-tabs-mode: nil
;; End:
