{
  pkgs ? import <nixpkgs> { },
}:

pkgs.mkShell {

  # Quests build errors with GCC.
  hardeningDisable = [ "all" ];

  name = "BL";
  packages = [
    # Basics.
    # pkgs.gcc-unwrapped
    pkgs.gcc14Stdenv
    pkgs.gcc14
    # pkgs.mold

    pkgs.cmakeCurses
    pkgs.ninja
    pkgs.cmake
    pkgs.dash # compiler wrapper uses this.
    # pkgs.pkg-config # Needed to execute directly.
    # For libasan.so
  ];
  buildInputs = [
    pkgs.pkg-config # Needed to execute directly.
    pkgs.gcc14Stdenv
    pkgs.gcc14
    # pkgs.gcc-unwrapped
    pkgs.libjpeg
    pkgs.zlib
    pkgs.libpng
    pkgs.zstd
    pkgs.libepoxy
    pkgs.freetype
    pkgs.python312
    pkgs.openimageio
    pkgs.brotli # For freetype.

    pkgs.egl-wayland
    pkgs.wayland
    pkgs.wayland-protocols
    pkgs.libxkbcommon

    pkgs.mesa
    pkgs.libglvnd # EGL/eglplatform.h

    # vulkan-headers
    pkgs.vulkan-loader # `vulkan` library.
    # vulkan-validation-layers
    # vulkan-tools        # vulkaninfo
    pkgs.shaderc

    pkgs.valgrind
    pkgs.imath

    pkgs.xorg.libX11
    pkgs.xorg.libXfixes
    pkgs.xorg.libXrender
  ];

  shellHook = ''
    # Some bash command and export some env vars.
    export I42_PS_PREFIX="[BL]"

    # For `libasan` to work for build time executables.
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${
      pkgs.lib.makeLibraryPath [
        # pkgs.gcc-unwrapped # For `libasan.so`.
        pkgs.libxkbcommon
        pkgs.openimageio
        pkgs.libjpeg
        pkgs.python312
        pkgs.epoxy
        pkgs.vulkan-loader
        pkgs.shaderc
        pkgs.freetype
        pkgs.libz
        pkgs.zstd
        pkgs.wayland
      ]
    };
  '';
}
