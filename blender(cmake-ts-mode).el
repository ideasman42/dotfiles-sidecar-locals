;; -*- lexical-binding: t -*-

;; Shared code for CMake-like languages.
(let ((path (file-name-directory load-file-name)))
  (load (concat path "blender(cmake-mode).el") :nomessage t))

;; Local Variables:
;; fill-column: 99
;; indent-tabs-mode: nil
;; End:
