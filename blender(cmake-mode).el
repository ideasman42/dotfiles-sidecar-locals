;; -*- lexical-binding: t -*-

(with-eval-after-load 'hl-prog-extra
  (setq hl-prog-extra-list

        ;; Extend the default list to include the bug tracker ID.
        (append
         (list

          ;; Match *some text* as bold.
          (list
           "\\*\\_<[^\\*]+\\_>\\*" 0 'comment-only '(:inherit font-lock-comment-face :weight bold))
          (list "\\*\\_<[^\\*]+\\_>\\*" 0 'comment-doc '(:inherit font-lock-doc-face :weight bold))

          ;; Match `some.text` as a constant.
          '("`[^`\n]+`" 0 comment font-lock-constant-face)

          '("[#!][[:digit:]]+\\>" 0 (string comment) font-lock-delimit-face))
         hl-prog-extra-list))
  (hl-prog-extra-refresh))

;; Local Variables:
;; fill-column: 99
;; indent-tabs-mode: nil
;; End:
