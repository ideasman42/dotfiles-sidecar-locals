;; -*- lexical-binding: t -*-

;; (setq mono-complete-backend-word-predict-input-paths (list "/src/manual/"))
;; (setq mono-complete-backends
;;   (lambda (is-context)
;;     (list (mono-complete--backend-load 'word-predict) (mono-complete--backend-load 'dabbrev))))
;; (mono-complete-mode)

(setq-local my-generic-build
            (lambda ()
              (interactive)
              (let
                  ( ;; ensure 'compile-command' is used.
                   (compilation-read-command nil)
                   (compile-command (concat "make -C " (sidecar-locals-root))))
                (call-interactively 'compile))))

;; Local Variables:
;; fill-column: 99
;; indent-tabs-mode: nil
;; End:
