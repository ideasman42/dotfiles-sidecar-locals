;; -*- lexical-binding: t -*-

(with-eval-after-load 'git-commit
  (when (bound-and-true-p git-commit-mode)
    (setq git-commit-summary-max-length fill-column)))

;; Local Variables:
;; fill-column: 99
;; indent-tabs-mode: nil
;; End:
